// Create a function that takes a Roman numeral as its argument and returns its value as a numeric decimal integer.
// You don't need to validate the form of the Roman numeral.

// Modern Roman numerals are written by expressing each decimal digit of the number to be encoded separately,
// starting with the leftmost digit and skipping any 0s.
// So 1990 is rendered "MCMXC" (1000 = M, 900 = CM, 90 = XC) and 2008 is rendered "MMVIII" (2000 = MM, 8 = VIII).
// The Roman numeral for 1666, "MDCLXVI", uses each letter in descending order.

// solution('XXI'); // should return 21

// Symbol    Value
// I          1
// V          5
// X          10
// L          50
// C          100
// D          500
// M          1,000

// 1 = I. 2 = II. 3 = III. 4 = IV. 5 = V. 6 = VI. 7 = VII. 8 = VIII. 9 = IX. 10 = X. 20 = XX.
// 100 = C. 200 = CC. 300 = CCC. 400 = CD. 500 = D. 600 = DC. 700 = DCC. 800 = DCCC. 900 = CM. 1 000 = M.

function solution(roman) {
  let result = 0;
  const arr = roman.split('');

  arr.map((item, index) => {
    const next = arr[index + 1];
    const prev = arr[index - 1];

    switch (item) {
      case 'I':
        result = result + 1;

        if (next === 'V') {
          result = result - 1;
        } else if (next === 'X') {
          result = result - 1;
        }

        break;
      case 'V':
        result = result + 5;

        if (prev === 'I') {
          result = result - 1;
        }

        break;
      case 'X':
        result = result + 10;

        if (prev === 'I') {
          result = result - 1;
        }

        if (next === 'C') {
          result = result - 10;
        }

        break;
      case 'L':
        result = result + 50;
        break;
      case 'C':
        result = result + 100;

        if (prev === 'X') {
          result = result - 10;
        }

        if (next === 'M') {
          result = result - 100;
        }

        break;
      case 'D':
        result = result + 500;

        break;
      case 'M':
        result = result + 1000;

        if (prev === 'C') {
          result = result - 100;
        }
        break;
    }
  })

  return result;
}

solution('XXI')
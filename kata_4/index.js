// Encrypt this!
//
//   You want to create secret messages which can be deciphered by the Decipher this! kata. Here are the conditions:
//
//   Your message is a string containing space separated words.
//   You need to encrypt each word in the message using the following rules:
//   The first letter must be converted to its ASCII code.
//   The second letter must be switched with the last letter
// Keepin' it simple: There are no special characters in the input.

const encryptThis = (str) => {
  const encryptWordsArr = [];
  const words = str.split(' ');

  words.map(word => {
    const letters = word.split('');

    letters.map((letter, i) => {
      if (i === 0) {
        letters[0] = letter.charCodeAt(0);
      } else if (i === 1) {
        [letters[1], letters[letters.length - 1]] = [letters[letters.length - 1], letters[1]];
      }
    })

    encryptWordsArr.push(letters.join(''));
  })

  return encryptWordsArr.join(' ');
}

encryptThis("Thank you Piotr for all your help") // "84kanh 121uo 80roti 102ro 97ll 121ruo 104ple"
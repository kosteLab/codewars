// Write a simple parser that will parse and run Deadfish.
//
//   Deadfish has 4 commands, each 1 character long:
//
//   i increments the value (initially 0)
// d decrements the value
// s squares the value
// o outputs the value into the return array
// Invalid characters should be ignored.

// parse("iiisdoso") => [8, 64]

const parse = (data) => {
  let current = 0;
  const result = [];
  data.split('').map(item => {
    switch (item) {
      case 'i':
        current++;
        break;
      case 'd':
        current--;
        break;
      case 's':
        current = Math.pow(current, 2);
        break;
      case 'o':
        result.push(current);
        break;
    }
  })

  return result;
}

parse('iiisxxxdoso')